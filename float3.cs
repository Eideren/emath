﻿// ReSharper disable InconsistentNaming
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global

#if UNITY_4 || UNITY_5 || UNITY_5_3_OR_NEWER
#define USING_UNITY
#endif
// Comment out to disable inlining
#define FORCE_INLINING

using Type = System.Type;
using InvalidOperationException = System.InvalidOperationException;
using ArgumentOutOfRangeException = System.ArgumentOutOfRangeException;
using ObsoleteAttribute = System.ObsoleteAttribute;

#if FORCE_INLINING
using Implementation = System.Runtime.CompilerServices.MethodImplAttribute;
using Options = System.Runtime.CompilerServices.MethodImplOptions;
#endif

#if USING_UNITY
using UnityEngine;
#endif


namespace E.EMath
{
	public struct float3 : IVector<float>
	{
		public float x, y, z;


		public float2 xx => new float2(x, x);
		public float2 yy => new float2(y, y);
		public float2 zz => new float2(z, z);

		public float2 xy => new float2(x, y);
		public float2 yx => new float2(y, x);
		public float2 xz => new float2(x, z);
		public float2 zx => new float2(z, x);
		public float2 yz => new float2(y, z);
		public float2 zy => new float2(z, y);

		public float3 xxx => new float3(x, x, x);
		public float3 yyy => new float3(y, y, y);
		public float3 zzz => new float3(z, z, z);

		public float3 xyz => new float3(x, y, z);
		public float3 xzy => new float3(x, z, y);
		public float3 yxz => new float3(y, x, z);
		public float3 yzx => new float3(y, z, x);
		public float3 zxy => new float3(z, x, y);
		public float3 zyx => new float3(z, y, x);



		public int Dimensions => 3;

		public float this[int v]
		{
			get
			{
				if      (v == 0)return x;
				else if (v == 1)return y;
				else if (v == 2)return z;
				else throw new ArgumentOutOfRangeException(v.ToString());
			}
			set
			{
				if      (v == 0) x = value;
				else if (v == 1) y = value;
				else if (v == 2) z = value;
				else throw new ArgumentOutOfRangeException(v.ToString());
			}
		}

		object IVector.this[int v]
		{
			get => this[v];
			set => this[v] = (float) value;
		}

		Type IVector.Type => typeof(float);

		#region CONSTRUCTOR

		public float3(float value)
		{
			x = value;
			y = value;
			z = value;
		}

		public float3(float newX, float newY, float newZ)
		{
			x = newX;
			y = newY;
			z = newZ;
		}

		public float3((float x, float y, float z) tuple)
		{
			x = tuple.x;
			y = tuple.y;
			z = tuple.z;
		}

		#endregion




		#region OPERATOR

		[Implementation(Inline)]
		public static float3 operator +(float3 a, float3 b)
		{
			return new float3(a.x + b.x, a.y + b.y, a.z + b.z);
		}

		[Implementation(Inline)]
		public static float3 operator -(float3 a, float3 b)
		{
			return new float3(a.x - b.x, a.y - b.y, a.z - b.z);
		}
		[Implementation(Inline)]
		public static float3 operator -(float3 a)
		{
			return new float3(-a.x, -a.y, -a.z);
		}

		[Implementation(Inline)]
		public static float3 operator *(float3 a, float3 b)
		{
			return new float3(a.x * b.x, a.y * b.y, a.z * b.z);
		}
		[Implementation(Inline)]
		public static float3 operator *(float3 a, float m)
		{
			return new float3(a.x * m, a.y * m, a.z * m);
		}

		[Implementation(Inline)]
		public static float3 operator *(float m, float3 a)
		{
			return new float3(a.x * m, a.y * m, a.z * m);
		}

		[Implementation(Inline)]
		public static float3 operator /(float3 a, float3 b)
		{
			return new float3(a.x / b.x, a.y / b.y, a.z / b.z);
		}
		[Implementation(Inline)]
		public static float3 operator /(float3 a, float d)
		{
			return new float3(a.x / d, a.y / d, a.z / d);
		}

		#endregion



		#region EQUALITY

		[Implementation(Inline)]
		public static bool operator ==(float3 a, float3 b)
		{
			// We expect exact equality when using this operator, do not round it.
			return a.x == b.x && a.y == b.y && a.z == b.z;
		}

		[Implementation(Inline)]
		public static bool operator !=(float3 a, float3 b)
		{
			// We expect exact inequality when using this operator, do not round it.
			return a.x != b.x || a.y != b.y || a.z != b.z;
		}

		public override bool Equals(object other)
		{
			if (other is float3 float3)
				return this == float3;
			else
				return false;
		}

		#endregion



		#region CONVERSION

		[Implementation(Inline)]
		public static implicit operator (float x, float y, float z)( float3 f3 )
		{
			return (f3.x, f3.y, f3.z);
		}
		[Implementation(Inline)]
		public static implicit operator float3( (float x, float y, float z) t3 )
		{
			return new float3(t3.x, t3.y, t3.z);
		}
		[Implementation(Inline)]
		public static explicit operator float3( float value )
		{
			return new float3(value, value, value);
		}

		#if USING_UNITY

		[Implementation(Inline)]
		public static implicit operator float3( Vector3 value )
		{
			return new float3(value.x, value.y, value.z);
		}

		[Implementation(Inline)]
		public static implicit operator Vector3( float3 value )
		{

			return new Vector3(value.x, value.y, value.z);
		}

		#endif


		#endregion


		public override string ToString() => $"({x}, {y}, {z})";

		public string ToString(string format) => $"({x.ToString(format)}, {y.ToString(format)}, {z.ToString(format)})";

		#pragma warning disable 0809
		/// <summary>
		/// This struct's x and y values aren't readonly, if they change our hash will as well and hashtables expect
		/// constant hash for objects. So convert/cast your float2 to float2f and use that instead.
		/// </summary>
		[ObsoleteAttribute("Convert/cast your float3 to float3f instead, this type is not hashtable safe")]
		public override int GetHashCode() => throw new InvalidOperationException($"Use/convert to {nameof(float3f)} instead, this type is not hashtable safe");
		#pragma warning restore 0809

		/// <summary>
		/// Shortcut for code readability
		/// </summary>
		const Options Inline = Options.AggressiveInlining;
	}

	/// <summary>
	/// Readonly version of float3, ONLY used to fetch safe HashCodes. To create one you can cast your float3 to float3f.
	/// </summary>
	public struct float3f
	{
		readonly float x, y, z;

		float3f(float newX, float newY, float newZ)
		{
			x = newX;
			y = newY;
			z = newZ;
		}

		[Implementation(Inline)]
		public static implicit operator float3( float3f v )
		{
			return new float3(v.x, v.y, v.z);
		}
		[Implementation(Inline)]
		public static implicit operator float3f( float3 v )
		{
			return new float3f(v.x, v.y, v.z);
		}

		public override int GetHashCode() => x.GetHashCode() ^ (y.GetHashCode() << 2);

		/// <summary>
		/// Shortcut for code readability
		/// </summary>
		const Options Inline = Options.AggressiveInlining;
	}

	public static class Float3
	{
		/// <summary>
		/// Shortcut for code readability
		/// </summary>
		const Options Inline = Options.AggressiveInlining;

		/// <summary>
		/// Returns the length of a vector ; the distance between it and (0, 0)
		/// </summary>
		[Implementation(Inline)]
		public static float Length(this float3 f2)
		{
			return Float.Sqrt(SquaredLength(f2));
		}

		/// <summary>
		/// Returns the squared length of a vector, faster than Length() but the returned value is not the length in
		/// unit and as such is mostly used for fast comparison between multiple vectors.
		/// </summary>
		[Implementation(Inline)]
		public static float SquaredLength(this float3 v)
		{
			return (v.x * v.x + v.y * v.y + v.z * v.z);
		}

		/// <summary>
		/// Returns the dot product between two vectors. Often used with normalized vectors to find out wheter they
		/// point to the same direction.
		/// </summary>
		[Implementation(Inline)]
		public static float Dot(this float3 a, float3 b)
		{
			return a.x * b.x + a.y * b.y + a.z * b.z;
		}

		/// <summary>
		/// Returns a vector going in the same direction with a length of one.
		/// </summary>
		[Implementation(Inline)]
		public static float3 Normalized(this float3 f3)
		{
			float length = SquaredLength(f3);
			return length > 0f ? f3 / Float.Sqrt(length) : new float3(0f);
		}

		/// <summary>
		/// Returns the same vector if its length is smaller than maxLength, if it is larger returns it with a length of maxLength.
		/// </summary>
		//[Implementation(Inline)]
		public static float3 MaxLength(this float3 vector, float maxLength)
		{
			if(maxLength == 0f)
				return new float3(0f, 0f, 0f);

			float sqrLength = vector.SquaredLength();
			if (sqrLength == 0f)
				return new float3(0f, 0f, 0f);

			if (sqrLength > maxLength * maxLength)
				return vector / (Float.Sqrt(sqrLength)) * maxLength;
			else
				return vector;
		}

		/// <summary>
		/// Returns the real distance in unit between those two points.
		/// </summary>
		[Implementation(Inline)]
		public static float Distance(this float3 a, float3 b)
		{
			return Length(a - b);
		}

		/// <summary>
		/// Returns a vector where each values of the source vector are between min and max.
		/// </summary>
		[Implementation(Inline)]
		public static float3 Clamped(this float3 src, float min, float max)
		{
			return new float3(src.x > max ? max : src.x < min ? min : src.x,
			                  src.y > max ? max : src.y < min ? min : src.y,
			                  src.z > max ? max : src.z < min ? min : src.z);
		}

		/// <summary>
		/// Returns a vector whose values are made absolute ; will swap to the positive value when it's negative.
		/// </summary>
		[Implementation(Inline)]
		public static float3 Absolute(this float3 src)
		{
			return new float3(src.x < 0f ? -src.x : src.x,
			                  src.y < 0f ? -src.y : src.y,
			                  src.z < 0f ? -src.z : src.z);
		}

		/// <summary>
		/// Returns a point on a line between 'zero' and 'one' based on 't''s value from 0 to 1. If 't' isn't between those
		/// values, the point will lay further along that line, outside of the segment zero->one.
		/// </summary>
		[Implementation(Inline)]
		public static float3 Lerp(float3 zero, float3 one, float t)
		{
			return new float3(zero.x + (one.x - zero.x) * t,
			                  zero.y + (one.y - zero.y) * t,
			                  zero.z + (one.z - zero.z) * t);
		}

		/// <summary>
		/// Moves a point 'current' 'stepSizes' unit towards 'target' point.
		/// </summary>
		//[Implementation(Inline)]
		public static float3 MoveTowards(this float3 current, float3 target, float stepSize)
		{
			float3 delta = target - current;
			float distanceSqrd = SquaredLength(delta);
			if (distanceSqrd > (stepSize * stepSize) && distanceSqrd > 0f)
				return current + ( delta / Float.Sqrt(distanceSqrd) ) * stepSize;
			else
				return target;
		}

		/// <summary>
		/// Returns a vector perpendicular (which has an angle of exactly 90 degrees) to the two parameters, the returned vector is not normalized.
		/// </summary>
		[Implementation(Inline)]
		public static float3 Cross(this float3 left, float3 right)
		{
			return new float3(left.y * right.z - left.z * right.y,
			                  left.z * right.x - left.x * right.z,
			                  left.x * right.y - left.y * right.x);
		}
	}
}