﻿// Comment out to disable inlining
#define FORCE_INLINING


using System;








namespace E.EMath
{
	#if FORCE_INLINING


	#else
	/// <summary>
	/// Dummy class to circumvent inlining when required
	/// </summary>
	public class ImplementationAttribute : Attribute
	{
		public ImplementationAttribute(Options mio){}
	}

	public enum Options
	{
		AggressiveInlining
	}
	#endif





	public interface IVector<T> : IVector
	{
		new T this[int i] { get; set; }
	}
	public interface IVector
	{
		int Dimensions { get; }
		object this[int i] { get; set; }
		Type Type { get; }
	}





	public struct Vector2<T> : IVector<T>
	{
		public T x, y;

		public int Dimensions => 2;

		public T this[int i]
		{
			get
			{
				if (i      == 0) return x;
				else if (i == 1) return y;
				else throw new IndexOutOfRangeException();
			}
			set
			{
				if (i      == 0) x = value;
				else if (i == 1) y = value;
				else throw new IndexOutOfRangeException();
			}
		}

		object IVector.this[int i]
		{
			get => this[i];
			set => this[i] = (T)value;
		}

		public Type Type => typeof(T);


		public Vector2(T newX, T newY)
		{
			x = newX;
			y = newY;
		}
	}
	public struct Vector3<T> : IVector<T>
	{
		public T x, y, z;

		public int Dimensions => 3;

		public T this[int i]
		{
			get
			{
				if (i      == 0) return x;
				else if (i == 1) return y;
				else if (i == 2) return z;
				else throw new IndexOutOfRangeException();
			}
			set
			{
				if (i      == 0) x = value;
				else if (i == 1) y = value;
				else if (i == 2) z = value;
				else throw new IndexOutOfRangeException();
			}
		}

		object IVector.this[int i]
		{
			get => this[i];
			set => this[i] = (T)value;
		}

		public Type Type => typeof(T);


		public Vector3(T newX, T newY, T newZ)
		{
			x = newX;
			y = newY;
			z = newZ;
		}
	}
}