﻿// ReSharper disable InconsistentNaming
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global

// Comment out to disable eto forms PointF support
//#define ETOPOINTF_SUPPORT
#if ETOPOINTF_SUPPORT
using Eto.Drawing;
#endif

#if UNITY_4 || UNITY_5 || UNITY_5_3_OR_NEWER
#define USING_UNITY
#endif

// Comment out to disable inlining
#define FORCE_INLINING

using Type = System.Type;
using InvalidOperationException = System.InvalidOperationException;
using ArgumentOutOfRangeException = System.ArgumentOutOfRangeException;
using ObsoleteAttribute = System.ObsoleteAttribute;

#if FORCE_INLINING
using Implementation = System.Runtime.CompilerServices.MethodImplAttribute;
using Options = System.Runtime.CompilerServices.MethodImplOptions;
#endif

#if USING_UNITY
using UnityEngine;
#endif








namespace E.EMath
{
	public struct int2 : IVector<int>
	{
		public int x, y;

		/// <summary>
		/// Gets this vector's value over a new vector with swapped axis
		/// </summary>
		public int2 xy => new int2(x, y);

		/// <summary>
		/// Gets this vector's value over a new vector with swapped axis
		/// </summary>
		public int2 yx => new int2(y, x);

		/// <summary>
		/// Gets this vector's x value over a new vector on both axis
		/// </summary>
		public int2 xx => new int2(x, x);

		/// <summary>
		/// Gets this vector's y value over a new vector on both axis
		/// </summary>
		public int2 yy => new int2(y, y);

		public int Dimensions => 2;

		public int this[int v]
		{
			get
			{
				if      (v == 0) return x;
				else if (v == 1) return y;
				else throw new ArgumentOutOfRangeException(v.ToString());
			}
			set
			{
				if      (v == 0) x = value;
				else if (v == 1) y = value;
				else throw new ArgumentOutOfRangeException(v.ToString());
			}
		}

		object IVector.this[int v]
		{
			get => this[v];
			set => this[v] = (int) value;
		}

		Type IVector.Type => typeof(int);

		#region CONSTRUCTOR

		public int2(int value)
		{
			x = value;
			y = value;
		}

		public int2(int newX, int newY)
		{
			x = newX;
			y = newY;
		}

		public int2((int x, int y)tuple)
		{
			x = tuple.x;
			y = tuple.y;
		}

		#endregion




		#region OPERATOR

		[Implementation(Inline)]
		public static int2 operator +(int2 a, int2 b)
		{
			return new int2(a.x + b.x, a.y + b.y);
		}

		[Implementation(Inline)]
		public static int2 operator -(int2 a, int2 b)
		{
			return new int2(a.x - b.x, a.y - b.y);
		}
		[Implementation(Inline)]
		public static int2 operator -(int2 a)
		{
			return new int2(-a.x, -a.y);
		}

		[Implementation(Inline)]
		public static int2 operator *(int2 a, int2 b)
		{
			return new int2(a.x * b.x, a.y * b.y);
		}
		[Implementation(Inline)]
		public static int2 operator *(int2 a, int m)
		{
			return new int2(a.x * m, a.y * m);
		}

		[Implementation(Inline)]
		public static int2 operator *(int m, int2 a)
		{
			return new int2(a.x * m, a.y * m);
		}

		[Implementation(Inline)]
		public static int2 operator /(int2 a, int2 b)
		{
			return new int2(a.x / b.x, a.y / b.y);
		}
		[Implementation(Inline)]
		public static int2 operator /(int2 a, int d)
		{
			return new int2(a.x / d, a.y / d);
		}

		#endregion



		#region EQUALITY

		[Implementation(Inline)]
		public static bool operator ==(int2 a, int2 b)
		{
			// We expect exact equality when using this operator, do not round it.
			return a.x == b.x && a.y == b.y;
		}

		[Implementation(Inline)]
		public static bool operator !=(int2 a, int2 b)
		{
			// We expect exact inequality when using this operator, do not round it.
			return a.x != b.x || a.y != b.y;
		}

		public override bool Equals(object other)
		{
			if (other is int2 int2)
				return this == int2;
			else
				return false;
		}

		#endregion



		#region CONVERSION

		[Implementation(Inline)]
		public static implicit operator (int x, int y)( int2 f2 )
		{
			return (f2.x, f2.y);
		}
		[Implementation(Inline)]
		public static implicit operator int2( (int x, int y) t2 )
		{
			return new int2(t2.x, t2.y);
		}
		[Implementation(Inline)]
		public static explicit operator int2( int value )
		{
			return new int2(value, value);
		}

		#if ETOPOINTF_SUPPORT
		[Implementation(Inline)]
		public static implicit operator PointF( int2 f2 )
		{
			return new PointF(f2.x, f2.y);
		}
		[Implementation(Inline)]
		public static implicit operator int2( PointF pf )
		{
			return new int2(pf.X, pf.Y);
		}
		#endif

		#if USING_UNITY

		[Implementation(Inline)]
		public static implicit operator int2( Vector2Int value )
		{
			return new int2(value.x, value.y);
		}

		[Implementation(Inline)]
		public static implicit operator Vector2Int( int2 value )
		{
			return new Vector2Int(value.x, value.y);
		}

		#endif

		#endregion












		public override string ToString() => $"({x}, {y})";

		public string ToString(string format) => $"({x.ToString(format)}, {y.ToString(format)})";

		#pragma warning disable 0809
		/// <summary>
		/// This struct's x and y values aren't readonly, if they change our hash will as well and hashtables expect
		/// constant hash for objects. So convert/cast your int2 to int2f and use that instead.
		/// </summary>
		[ObsoleteAttribute("Convert/cast your int2 to int2f instead, this type is not hashtable safe")]
		public override int GetHashCode() => throw new InvalidOperationException($"Use/convert to {nameof(int2f)} instead, this type is not hashtable safe");
		#pragma warning restore 0809

		/// <summary>
		/// Shortcut for code readability
		/// </summary>
		const Options Inline = Options.AggressiveInlining;
	}





	/// <summary>
	/// Readonly version of int2, ONLY used to fetch safe HashCodes. To create one you can cast your int2 to int2f.
	/// </summary>
	public struct int2f
	{
		readonly int x, y;

		int2f(int newX, int newY)
		{
			x = newX;
			y = newY;
		}

		[Implementation(Inline)]
		public static implicit operator int2( int2f f2f )
		{
			return new int2(f2f.x, f2f.y);
		}
		[Implementation(Inline)]
		public static implicit operator int2f( int2 f2 )
		{
			return new int2f(f2.x, f2.y);
		}

		public override int GetHashCode() => x.GetHashCode() ^ (y.GetHashCode() << 2);

		/// <summary>
		/// Shortcut for code readability
		/// </summary>
		const Options Inline = Options.AggressiveInlining;
	}



	public static class Int2
	{
		/// <summary>
		/// Shortcut for code readability
		/// </summary>
		const Options Inline = Options.AggressiveInlining;

		/// <summary>
		/// Returns a vector where each values of the source vector are between min and max.
		/// </summary>
		[Implementation(Inline)]
		public static int2 Clamped(this int2 src, int min, int max)
		{
			return new int2(src.x > max ? max : src.x < min ? min : src.x,
			                  src.y > max ? max : src.y < min ? min : src.y);
		}

		/// <summary>
		/// Returns a vector whose values are made absolute ; will swap to the positive value when it's negative.
		/// </summary>
		[Implementation(Inline)]
		public static int2 Absolute(this int2 src)
		{
			return new int2(src.x < 0f ? -src.x : src.x,
			                  src.y < 0f ? -src.y : src.y);
		}
	}
}