﻿
// Comment out to disable inlining
#define FORCE_INLINING
#if FORCE_INLINING
using Implementation = System.Runtime.CompilerServices.MethodImplAttribute;
using Options = System.Runtime.CompilerServices.MethodImplOptions;
#endif

using Math = System.Math;




namespace E.EMath
{
	public static class Float
	{
		[Implementation(Inline)]
		public static float Sqrt(float v)
		{
			return (float)Math.Sqrt(v);
		}





















		/// <summary>
		/// Shortcut for code readability
		/// </summary>
		const Options Inline = Options.AggressiveInlining;
	}
}