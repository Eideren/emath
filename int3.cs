﻿// ReSharper disable InconsistentNaming
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global

#if UNITY_4 || UNITY_5 || UNITY_5_3_OR_NEWER
#define USING_UNITY
#endif
// Comment out to disable inlining
#define FORCE_INLINING

using Type = System.Type;
using InvalidOperationException = System.InvalidOperationException;
using ArgumentOutOfRangeException = System.ArgumentOutOfRangeException;
using ObsoleteAttribute = System.ObsoleteAttribute;

#if FORCE_INLINING
using Implementation = System.Runtime.CompilerServices.MethodImplAttribute;
using Options = System.Runtime.CompilerServices.MethodImplOptions;
#endif

#if USING_UNITY
using UnityEngine;
#endif


namespace E.EMath
{
	public struct int3 : IVector<int>
	{
		public int x, y, z;


		public int2 xx => new int2(x, x);
		public int2 yy => new int2(y, y);
		public int2 zz => new int2(z, z);

		public int2 xy => new int2(x, y);
		public int2 yx => new int2(y, x);
		public int2 xz => new int2(x, z);
		public int2 zx => new int2(z, x);
		public int2 yz => new int2(y, z);
		public int2 zy => new int2(z, y);

		public int3 xxx => new int3(x, x, x);
		public int3 yyy => new int3(y, y, y);
		public int3 zzz => new int3(z, z, z);

		public int3 xyz => new int3(x, y, z);
		public int3 xzy => new int3(x, z, y);
		public int3 yxz => new int3(y, x, z);
		public int3 yzx => new int3(y, z, x);
		public int3 zxy => new int3(z, x, y);
		public int3 zyx => new int3(z, y, x);



		public int Dimensions => 3;

		public int this[int v]
		{
			get
			{
				if      (v == 0)return x;
				else if (v == 1)return y;
				else if (v == 2)return z;
				else throw new ArgumentOutOfRangeException(v.ToString());
			}
			set
			{
				if      (v == 0) x = value;
				else if (v == 1) y = value;
				else if (v == 2) z = value;
				else throw new ArgumentOutOfRangeException(v.ToString());
			}
		}

		object IVector.this[int v]
		{
			get => this[v];
			set => this[v] = (int) value;
		}

		Type IVector.Type => typeof(int);

		#region CONSTRUCTOR

		public int3(int value)
		{
			x = value;
			y = value;
			z = value;
		}

		public int3(int newX, int newY, int newZ)
		{
			x = newX;
			y = newY;
			z = newZ;
		}

		public int3((int x, int y, int z) tuple)
		{
			x = tuple.x;
			y = tuple.y;
			z = tuple.z;
		}

		#endregion




		#region OPERATOR

		[Implementation(Inline)]
		public static int3 operator +(int3 a, int3 b)
		{
			return new int3(a.x + b.x, a.y + b.y, a.z + b.z);
		}

		[Implementation(Inline)]
		public static int3 operator -(int3 a, int3 b)
		{
			return new int3(a.x - b.x, a.y - b.y, a.z - b.z);
		}
		[Implementation(Inline)]
		public static int3 operator -(int3 a)
		{
			return new int3(-a.x, -a.y, -a.z);
		}

		[Implementation(Inline)]
		public static int3 operator *(int3 a, int3 b)
		{
			return new int3(a.x * b.x, a.y * b.y, a.z * b.z);
		}
		[Implementation(Inline)]
		public static int3 operator *(int3 a, int m)
		{
			return new int3(a.x * m, a.y * m, a.z * m);
		}

		[Implementation(Inline)]
		public static int3 operator *(int m, int3 a)
		{
			return new int3(a.x * m, a.y * m, a.z * m);
		}

		[Implementation(Inline)]
		public static int3 operator /(int3 a, int3 b)
		{
			return new int3(a.x / b.x, a.y / b.y, a.z / b.z);
		}
		[Implementation(Inline)]
		public static int3 operator /(int3 a, int d)
		{
			return new int3(a.x / d, a.y / d, a.z / d);
		}

		#endregion



		#region EQUALITY

		[Implementation(Inline)]
		public static bool operator ==(int3 a, int3 b)
		{
			// We expect exact equality when using this operator, do not round it.
			return a.x == b.x && a.y == b.y && a.z == b.z;
		}

		[Implementation(Inline)]
		public static bool operator !=(int3 a, int3 b)
		{
			// We expect exact inequality when using this operator, do not round it.
			return a.x != b.x || a.y != b.y || a.z != b.z;
		}

		public override bool Equals(object other)
		{
			if (other is int3 int3)
				return this == int3;
			else
				return false;
		}

		#endregion



		#region CONVERSION

		[Implementation(Inline)]
		public static implicit operator (int x, int y, int z)( int3 f3 )
		{
			return (f3.x, f3.y, f3.z);
		}
		[Implementation(Inline)]
		public static implicit operator int3( (int x, int y, int z) t3 )
		{
			return new int3(t3.x, t3.y, t3.z);
		}
		[Implementation(Inline)]
		public static explicit operator int3( int value )
		{
			return new int3(value, value, value);
		}

		#if USING_UNITY

		[Implementation(Inline)]
		public static implicit operator int3( Vector3Int value )
		{
			return new int3(value.x, value.y, value.z);
		}

		[Implementation(Inline)]
		public static implicit operator Vector3Int( int3 value )
		{

			return new Vector3Int(value.x, value.y, value.z);
		}

		#endif


		#endregion


		public override string ToString() => $"({x}, {y}, {z})";

		public string ToString(string format) => $"({x.ToString(format)}, {y.ToString(format)}, {z.ToString(format)})";

		#pragma warning disable 0809
		/// <summary>
		/// This struct's x and y values aren't readonly, if they change our hash will as well and hashtables expect
		/// constant hash for objects. So convert/cast your int2 to int2f and use that instead.
		/// </summary>
		[ObsoleteAttribute("Convert/cast your int3 to int3f instead, this type is not hashtable safe")]
		public override int GetHashCode() => throw new InvalidOperationException($"Use/convert to {nameof(int3f)} instead, this type is not hashtable safe");
		#pragma warning restore 0809

		/// <summary>
		/// Shortcut for code readability
		/// </summary>
		const Options Inline = Options.AggressiveInlining;
	}

	/// <summary>
	/// Readonly version of int3, ONLY used to fetch safe HashCodes. To create one you can cast your int3 to int3f.
	/// </summary>
	public struct int3f
	{
		readonly int x, y, z;

		int3f(int newX, int newY, int newZ)
		{
			x = newX;
			y = newY;
			z = newZ;
		}

		[Implementation(Inline)]
		public static implicit operator int3( int3f v )
		{
			return new int3(v.x, v.y, v.z);
		}
		[Implementation(Inline)]
		public static implicit operator int3f( int3 v )
		{
			return new int3f(v.x, v.y, v.z);
		}

		public override int GetHashCode() => x.GetHashCode() ^ (y.GetHashCode() << 2);

		/// <summary>
		/// Shortcut for code readability
		/// </summary>
		const Options Inline = Options.AggressiveInlining;
	}

	public static class Int3
	{
		/// <summary>
		/// Shortcut for code readability
		/// </summary>
		const Options Inline = Options.AggressiveInlining;

		/// <summary>
		/// Returns a vector where each values of the source vector are between min and max.
		/// </summary>
		[Implementation(Inline)]
		public static int3 Clamped(this int3 src, int min, int max)
		{
			return new int3(src.x > max ? max : src.x < min ? min : src.x,
			                  src.y > max ? max : src.y < min ? min : src.y,
			                  src.z > max ? max : src.z < min ? min : src.z);
		}

		/// <summary>
		/// Returns a vector whose values are made absolute ; will swap to the positive value when it's negative.
		/// </summary>
		[Implementation(Inline)]
		public static int3 Absolute(this int3 src)
		{
			return new int3(src.x < 0f ? -src.x : src.x,
			                  src.y < 0f ? -src.y : src.y,
			                  src.z < 0f ? -src.z : src.z);
		}
	}
}